﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using MyBlog.DAL;
using MyBlog.Models;
using MyBlog.ViewModels;

namespace MyBlog.Facade
{
    public class Facade
    {
        private AppDbContext context;

        public Facade()
        {
            this.context = new AppDbContext();
        }

        public List<UserViewModel> GetUserListWithRoleName()
        {
            List<ApplicationUser> applicationUsersList = context.Users.OrderByDescending(n => n.UserName).ToList();
            List<UserViewModel> list = new List<UserViewModel>();
            foreach (var x in applicationUsersList)
            {
                UserViewModel tmp = new UserViewModel();
                tmp.user = x;

                foreach (var role in x.Roles)
                {
                    IdentityRole tmpRole = context.Roles.SingleOrDefault(n => n.Id == role.RoleId);
                    tmp.roleName.Add(tmpRole.Name);
                }
                list.Add(tmp);
            }
            return list;
        }

        public List<Note> GetNotes()
        {
            return context.Notes.OrderByDescending(n => n.NoteId).ToList();
        }

        public Note GetNote(int id)
        {
            return context.Notes.Find(id);
        }

        public bool AddNote(Note note)
        {
            try
            {
                note.Date = DateTime.Now;
                context.Notes.Add(note);
                context.SaveChanges();
                return true;
            }
            catch
            {
                return false;
            }
        }

        public bool AddComment(CommentForNote comment)
        {
            try
            {
                comment.Date = DateTime.Now;
                context.Comments.Add(comment);
                context.SaveChanges();
                return true;
            }
            catch
            {
                return false;
            }
        }

        public void SetRoles(string userId, bool isAdmin, bool isEditor)
        {
            var UserManger = new UserManager<ApplicationUser>(new UserStore<ApplicationUser>(context));
            if (isAdmin)
            {
                UserManger.AddToRole(userId, "Admin");
            }
            else
            {
                UserManger.RemoveFromRole(userId, "Admin");
            }

            if (isEditor)
            {
                UserManger.AddToRole(userId, "Editor");
            }
            else
            {
                UserManger.RemoveFromRole(userId, "Editor");
            }
        }

        public void EditNote(Note note)
        {
            context.Notes.Attach(note);
            context.Entry(note).State = EntityState.Modified;
            context.SaveChanges();
        }

        public void DeleteComment(int commentId)
        {
            CommentForNote comment = context.Comments.Find(commentId);
            context.Comments.Attach(comment);
            context.Entry(comment).State = EntityState.Deleted;
            context.SaveChanges();
        }

        public void DeleteNote(int id)
        {
            Note note = context.Notes.Find(id);
            context.Notes.Attach(note);
            context.Entry(note).State = EntityState.Deleted;
            context.SaveChanges();
        }

        public UserEditRolesViewModel GetUserEditRolesViewModel(string userId)
        {
            UserViewModel user = new UserViewModel();
            user.user = context.Users.Find(userId);
            UserEditRolesViewModel userEditRolesViewModel = new UserEditRolesViewModel(user.user.UserName, userId);

            foreach (var role in user.user.Roles)
            {
                string roleName = context.Roles.SingleOrDefault(n => n.Id == role.RoleId).Name;
                switch (roleName)
                {
                    case "Admin":
                        userEditRolesViewModel.isAdmin = true;
                        break;
                    case "Editor":
                        userEditRolesViewModel.isEditor = true;
                        break;
                }
            }
            return userEditRolesViewModel;
        }

    }
}
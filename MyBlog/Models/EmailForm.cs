﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Dynamic;
using System.Linq;
using System.Web;

namespace MyBlog.Models
{
    public class EmailForm
    {
        [Required(ErrorMessage = "Musisz podać adres email")]
        [EmailAddress(ErrorMessage = "Błędny email")]
        [Display(Name = "Adres e-mail")]
        public string EmailSender { get; set; }

        [Required(ErrorMessage = "Musisz podać temat wiadomości")]
        [Display(Name = "Temat")]
        public string Subject { get; set; }

        [Required(ErrorMessage ="Musisz podać treść wiadomości" )]
        [Display(Name = "Treść")]
        [DataType(DataType.MultilineText)]
        public string Content { get; set; }
    }
}
﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Dynamic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace MyBlog.Models
{
    public class Note
    {
        [ScaffoldColumn(false)]
        public int NoteId { get; set; }

        [Required(ErrorMessage = "Musisz podać tytuł")]
        [Display(Name = "Tytuł")]
        public string Title { get; set; }

        [Required(ErrorMessage = "Musisz podać treść")]
        [Display(Name = "Treść")]
        [DataType(DataType.MultilineText)]
        [AllowHtml]
        public string Content { get; set; }

        [ScaffoldColumn(false)]
        public DateTime Date { get; set; }

        [ScaffoldColumn(false)]
        public string UserId { get; set; }
        public virtual ApplicationUser User { get; set; }

        public virtual ICollection<CommentForNote> Comments { get; set; }
    }
}
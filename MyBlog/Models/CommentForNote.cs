﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace MyBlog.Models
{
    public class CommentForNote
    {
        [Key]
        [ScaffoldColumn(false)]
        public int CommentId { get; set; }
        
        //[Required(ErrorMessage = "Musisz podac nick")]
        //[Display(Name = "Pseudonim")]
        //public string Nick { get; set; }

        [Required]
        [Display(Name = "Treść")]
        [DataType(DataType.MultilineText)]
        public string Content { get; set; }

        [ScaffoldColumn(false)]
        public DateTime Date { get; set; }

        [ScaffoldColumn(false)]
        public int NoteId { get; set; }
        public virtual Note Note { get; set; }

        [ScaffoldColumn(false)]
        public string UserId { get; set; }
        public virtual ApplicationUser User { get; set; }
    }
}
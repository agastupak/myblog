﻿

using System.Data.Entity;
using System.Web.Mvc;
using System.Web.Routing;
using MyBlog.DAL;

namespace MyBlog
{
    public class MvcApplication : System.Web.HttpApplication
    {
        protected void Application_Start()
        {
            AreaRegistration.RegisterAllAreas();
            RouteConfig.RegisterRoutes(RouteTable.Routes);
            //init database
            DbInitHandler.Initialize();
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace MyBlog.ViewModels
{
    public class UserEditRolesViewModel
    {
        [ScaffoldColumn(false)]
        [Display(Name = "Nazwa użytkownika")]
        public string UserName { get; set; }
        [ScaffoldColumn(false)]
        public string UserId { get; set; }

        [Display(Name = "Administrator")]
        public bool isAdmin { get; set; }
        [Display(Name = "Redaktor")]
        public bool isEditor { get; set; }

        public UserEditRolesViewModel(string userName, string userId)
        {
            UserName = userName;
            UserId = userId;
            isAdmin = false;
            isEditor = false;
        }

        public UserEditRolesViewModel()
        {
        }
    }
    
}
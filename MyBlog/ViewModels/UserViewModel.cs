﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Security;
using Microsoft.AspNet.Identity.EntityFramework;
using MyBlog.Models;

namespace MyBlog.ViewModels
{
    public class UserViewModel
    {
        public ApplicationUser user { get; set; }
        public List<string> roleName { get; set; }

        public UserViewModel()
        {
            this.roleName = new List<string>();
        }
    }
}
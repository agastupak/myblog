﻿using System.Data.Entity;

namespace MyBlog.DAL
{
    public static class DbInitHandler
    {
        public static void Initialize()
        {
            Database.SetInitializer(new AppDbInit()); //if u want to use your initializer
            using (var db = new AppDbContext())
            {
                {
                    db.Database.Initialize(true);
                }
            }
        }
    }
}
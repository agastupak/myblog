﻿

using System;
using System.Collections.Generic;
using System.Data.Entity;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using MyBlog.Models;

namespace MyBlog.DAL
{
    public class AppDbInit : DropCreateDatabaseAlways<AppDbContext>
    {
        protected override void Seed(AppDbContext context)
        {
            List<string> roleList = new List<string>()
           {
               "Admin",
               "Editor"
           };


            var RoleManager = new RoleManager<IdentityRole>(new RoleStore<IdentityRole>(context));

            foreach (string role in roleList)
            {
                if (!RoleManager.RoleExists(role))
                {
                    var roleresult = RoleManager.Create(new IdentityRole(role));
                }
            }

            string pasword = "123123";

            var UserManger = new UserManager<ApplicationUser>(new UserStore<ApplicationUser>(context));

            var userAdmin = new ApplicationUser() { UserName = "Admin", Email = "admin@blog.com" };
            if(UserManger.Create(userAdmin, pasword).Succeeded)
            {
                UserManger.AddToRole(userAdmin.Id, "Admin");
                UserManger.AddToRole(userAdmin.Id, "Editor");
            }

            var userEditor = new ApplicationUser() { UserName = "Editor", Email = "editor@blog.com" };
            if (UserManger.Create(userEditor, pasword).Succeeded)
            {
                UserManger.AddToRole(userEditor.Id, "Editor");
            }

            var member = new ApplicationUser() { UserName = "Member", Email = "member@blog.com" };
            if (UserManger.Create(member, pasword).Succeeded)
            {
                //UserManger.AddToRole(member.Id, "Member");
            }



            var notes = new List<Note>()
            {
                new Note(){UserId = userAdmin.Id, Title="Witam", Content = "To jest powitalny testowy wpis", Date = DateTime.Now, Comments = new List<CommentForNote>()
                {
                    new CommentForNote(){UserId = userEditor.Id, Content = "Test komentarza", Date = DateTime.Now},
                    new CommentForNote(){UserId = userEditor.Id, Content = "Jeszcze jeden testowy komentarz", Date = DateTime.Now}
                } },
                new Note(){UserId = userAdmin.Id, Title="O Blogu", Content = "Aplikację zbudowana została z wykorzystaniem ASP.NET MVC, ASP.NET Identity oraz Bootstrap. By uzyskać więcej informacji zapraszam do kontaktu.", Date = DateTime.Now}

            };

            notes.ForEach(n => context.Notes.Add(n));
            context.SaveChanges();

        }
    }
}
﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;
using Microsoft.AspNet.Identity.EntityFramework;
using MyBlog.Models;

namespace MyBlog.DAL
{
    public class AppDbContext : IdentityDbContext<ApplicationUser>
    {
        public AppDbContext()
            : base("MyConnectionString")
        {
        }

        //tutaj podpinasz do contextu swoje modele 
        // np. public DbSet<NazwaModelu> NazwaModelu { get; set; }

        public DbSet<Note> Notes { get; set; }
        public DbSet<CommentForNote> Comments { get; set; }


        public static AppDbContext Create()
        {
            return new AppDbContext();
        }
    }













}
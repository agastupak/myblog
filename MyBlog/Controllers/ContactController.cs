﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Net.Mail;
using System.Web;
using System.Web.Mvc;
using MyBlog.Models;

namespace MyBlog.Controllers
{
    public class ContactController : Controller
    {
        // GET: Contact
        public ActionResult SendEmail()
        {

            return View();
        }

        [HttpPost]
        public ActionResult SendEmail(EmailForm emailForm)
        {
            if (ModelState.IsValid)
            {

                SmtpClient client = new SmtpClient();
                client.DeliveryMethod = SmtpDeliveryMethod.Network;
                client.EnableSsl = true;
                client.Host = "smtp.gmail.com";
                client.Port = 587;

                // setup Smtp authentication
                System.Net.NetworkCredential credentials =
                    new System.Net.NetworkCredential("myzwirkablog", "zwirkasuperswinka");

                client.UseDefaultCredentials = false;
                client.Credentials = credentials;

                MailMessage msg = new MailMessage();
                msg.From = new MailAddress(emailForm.EmailSender);
                msg.To.Add(new MailAddress("hane.aga@gmail.com"));

                msg.Subject = emailForm.Subject;
                msg.IsBodyHtml = true;
                msg.Body = string.Format("<html><head></head><body><table><tr><td>Adresat: </td><td>"+ emailForm.EmailSender + "</td></tr><tr><td>" + emailForm.Content + "</td></tr></table></body>");
                try
                {
                    client.Send(msg);
                    ViewBag.Status = true;
                    ViewBag.Message = "Dziękuję za wiadomość. E-mail został wysłany!";
                }
                catch
                {
                    ViewBag.Status = false;
                    ViewBag.Message = "Wadimość nie została wysłana! Coś poszło nie tak:( Sprawdź konfigurację";
                }
            }
            else
            {
                ViewBag.Status = false;
                ViewBag.Message = "Nie udało się odczytać formularza, proszę sprawdź dane.";
            }

            return View();
        }
    }
}
﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Security;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using MyBlog.App_Start;
using MyBlog.DAL;
using MyBlog.Models;
using MyBlog.ViewModels;

namespace MyBlog.Controllers
{
    [Authorize(Roles = "Admin,Editor")]
    [AccessDeniedAuthorize(Roles = "Admin, Editor")]
    public class AdminController : Controller
    {
        // GET: Admin

        public ActionResult Index()
        {
            return View();
        }


        public ActionResult Notes()
        {
            List<Note> notesList = new Facade.Facade().GetNotes();
            return View(notesList);
        }


        public ActionResult EditNote(int id)
        {
            Note note = new Facade.Facade().GetNote(id);

            return View(note);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult EditNote(Note note)
        {
            new Facade.Facade().EditNote(note);
            return RedirectToAction("Notes", "Admin");
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteNote(int id)
        {
            new Facade.Facade().DeleteNote(id);
            return RedirectToAction("Notes", "Admin");
        }



        public ActionResult UserList()
        {
            return View(new Facade.Facade().GetUserListWithRoleName());
        }

        public ActionResult EditUserRole(string userId)
        {
            return View(new Facade.Facade().GetUserEditRolesViewModel(userId));
        }

        [HttpPost]
        public ActionResult SetUserRoles(string userId, bool isAdmin, bool isEditor)
        {
            new Facade.Facade().SetRoles(userId,isAdmin,isEditor);
            return RedirectToAction("UserList", "Admin");
        }


        
        [Authorize]
        [AccessDeniedAuthorize(Roles = "Admin, Editor")]
        public ActionResult AddNote()
        {
            return View();
        }

        [HttpPost]
        [AccessDeniedAuthorize(Roles = "Admin, Editor")]
        [ValidateAntiForgeryToken]
        public ActionResult AddNote(Note note)
        {

            if (ModelState.IsValid)
            {
                if (new Facade.Facade().AddNote(note) == true)
                {
                    ViewBag.Status = true;
                    ViewBag.Message = "Dodano notatkę";

                }
                else
                {
                    ViewBag.Status = false;
                    ViewBag.Message = "Nie dodano notatki. Spróbuj ponownie.";
                }
            }
            else
            {
                ViewBag.IsAdded = false;
                ViewBag.Message = "Nie dodano notatki. Spróbuj ponownie.";
            }
            return View();
        }



    }
}
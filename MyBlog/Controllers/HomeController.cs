﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using MyBlog.DAL;
using MyBlog.Models;
using PagedList;

namespace MyBlog.Controllers
{
    public class HomeController : Controller
    {
        // GET: Home
        public ActionResult Index(int page=1, int pageSize=5)
        {
            var notes = new PagedList<Note>(new Facade.Facade().GetNotes(),page, pageSize);
            return View(notes);
        }

    }
}
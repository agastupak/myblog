﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Security;
using MyBlog.App_Start;
using MyBlog.DAL;
using MyBlog.Models;

namespace MyBlog.Controllers
{
    public class NoteController : Controller
    {
        [Authorize]
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult AddComment(CommentForNote comment)
        {

            if (ModelState.IsValid)
            {
                new Facade.Facade().AddComment(comment);
            }
            return RedirectToAction("ShowNoteDetails", "Note", new {id = comment.NoteId});
        }


        public ActionResult ShowNoteDetails(int id)
        {
            return View(new Facade.Facade().GetNote(id));
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteComment(int commentId, int noteId)
        {
            new Facade.Facade().DeleteComment(commentId);
            return RedirectToAction("ShowNoteDetails", "Note", new { id = noteId });
        }

    }

    
}
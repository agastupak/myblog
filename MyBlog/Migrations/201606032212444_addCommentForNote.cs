namespace MyBlog.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class addCommentForNote : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.CommentForNotes",
                c => new
                    {
                        CommentId = c.Int(nullable: false, identity: true),
                        Nick = c.String(nullable: false),
                        Content = c.String(nullable: false),
                        NoteId = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.CommentId)
                .ForeignKey("dbo.Notes", t => t.NoteId, cascadeDelete: true)
                .Index(t => t.NoteId);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.CommentForNotes", "NoteId", "dbo.Notes");
            DropIndex("dbo.CommentForNotes", new[] { "NoteId" });
            DropTable("dbo.CommentForNotes");
        }
    }
}

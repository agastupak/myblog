namespace MyBlog.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class addCommentForNote2 : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.CommentForNotes", "Date", c => c.DateTime(nullable: false));
        }
        
        public override void Down()
        {
            DropColumn("dbo.CommentForNotes", "Date");
        }
    }
}

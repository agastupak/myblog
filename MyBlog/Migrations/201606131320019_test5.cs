namespace MyBlog.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class test5 : DbMigration
    {
        public override void Up()
        {
            DropForeignKey("dbo.AspNetRoles", new[] { "IdentityUserRole_UserId", "IdentityUserRole_RoleId" }, "dbo.AspNetUserRoles");
            DropForeignKey("dbo.AspNetUsers", "IdentityRole_Id", "dbo.AspNetRoles");
            DropIndex("dbo.AspNetUsers", new[] { "IdentityRole_Id" });
            DropIndex("dbo.AspNetRoles", new[] { "IdentityUserRole_UserId", "IdentityUserRole_RoleId" });
            DropColumn("dbo.AspNetUsers", "IdentityRole_Id");
            DropColumn("dbo.AspNetRoles", "Discriminator");
            DropColumn("dbo.AspNetRoles", "IdentityUserRole_UserId");
            DropColumn("dbo.AspNetRoles", "IdentityUserRole_RoleId");
        }
        
        public override void Down()
        {
            AddColumn("dbo.AspNetRoles", "IdentityUserRole_RoleId", c => c.String(maxLength: 128));
            AddColumn("dbo.AspNetRoles", "IdentityUserRole_UserId", c => c.String(maxLength: 128));
            AddColumn("dbo.AspNetRoles", "Discriminator", c => c.String(nullable: false, maxLength: 128));
            AddColumn("dbo.AspNetUsers", "IdentityRole_Id", c => c.String(maxLength: 128));
            CreateIndex("dbo.AspNetRoles", new[] { "IdentityUserRole_UserId", "IdentityUserRole_RoleId" });
            CreateIndex("dbo.AspNetUsers", "IdentityRole_Id");
            AddForeignKey("dbo.AspNetUsers", "IdentityRole_Id", "dbo.AspNetRoles", "Id");
            AddForeignKey("dbo.AspNetRoles", new[] { "IdentityUserRole_UserId", "IdentityUserRole_RoleId" }, "dbo.AspNetUserRoles", new[] { "UserId", "RoleId" });
        }
    }
}

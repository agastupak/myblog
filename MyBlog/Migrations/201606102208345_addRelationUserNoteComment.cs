namespace MyBlog.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class addRelationUserNoteComment : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.CommentForNotes", "UserId", c => c.String(maxLength: 128));
            CreateIndex("dbo.CommentForNotes", "UserId");
            AddForeignKey("dbo.CommentForNotes", "UserId", "dbo.AspNetUsers", "Id");
            DropColumn("dbo.CommentForNotes", "Nick");
        }
        
        public override void Down()
        {
            AddColumn("dbo.CommentForNotes", "Nick", c => c.String(nullable: false));
            DropForeignKey("dbo.CommentForNotes", "UserId", "dbo.AspNetUsers");
            DropIndex("dbo.CommentForNotes", new[] { "UserId" });
            DropColumn("dbo.CommentForNotes", "UserId");
        }
    }
}

namespace MyBlog.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class test3 : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.AspNetRoles", "Discriminator", c => c.String(nullable: false, maxLength: 128));
            AddColumn("dbo.AspNetRoles", "IdentityUserRole_UserId", c => c.String(maxLength: 128));
            AddColumn("dbo.AspNetRoles", "IdentityUserRole_RoleId", c => c.String(maxLength: 128));
            CreateIndex("dbo.AspNetRoles", new[] { "IdentityUserRole_UserId", "IdentityUserRole_RoleId" });
            AddForeignKey("dbo.AspNetRoles", new[] { "IdentityUserRole_UserId", "IdentityUserRole_RoleId" }, "dbo.AspNetUserRoles", new[] { "UserId", "RoleId" });
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.AspNetRoles", new[] { "IdentityUserRole_UserId", "IdentityUserRole_RoleId" }, "dbo.AspNetUserRoles");
            DropIndex("dbo.AspNetRoles", new[] { "IdentityUserRole_UserId", "IdentityUserRole_RoleId" });
            DropColumn("dbo.AspNetRoles", "IdentityUserRole_RoleId");
            DropColumn("dbo.AspNetRoles", "IdentityUserRole_UserId");
            DropColumn("dbo.AspNetRoles", "Discriminator");
        }
    }
}

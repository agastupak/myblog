namespace MyBlog.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class test2 : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.AspNetUsers", "IdentityRole_Id", c => c.String(maxLength: 128));
            CreateIndex("dbo.AspNetUsers", "IdentityRole_Id");
            AddForeignKey("dbo.AspNetUsers", "IdentityRole_Id", "dbo.AspNetRoles", "Id");
            DropColumn("dbo.AspNetRoles", "Test");
            DropColumn("dbo.AspNetRoles", "Discriminator");
        }
        
        public override void Down()
        {
            AddColumn("dbo.AspNetRoles", "Discriminator", c => c.String(nullable: false, maxLength: 128));
            AddColumn("dbo.AspNetRoles", "Test", c => c.String());
            DropForeignKey("dbo.AspNetUsers", "IdentityRole_Id", "dbo.AspNetRoles");
            DropIndex("dbo.AspNetUsers", new[] { "IdentityRole_Id" });
            DropColumn("dbo.AspNetUsers", "IdentityRole_Id");
        }
    }
}
